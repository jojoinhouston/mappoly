import {Component, OnInit} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-map-component',
  templateUrl: './map-component.component.html',
  styleUrls: ['./map-component.component.css']
})

export class MapComponentComponent implements OnInit {
  feature = '';
  features: any[] = [];
  private map: any;
  private draw: any;
  geojson: any;
  // tslint:disable-next-line:variable-name
  public name_save_button = false;
  // tslint:disable-next-line:variable-name
  geojson_name = '';

  constructor() {
  }

  ngOnInit(): void {
    (mapboxgl as typeof mapboxgl).accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 2,
      center: [-120, 53]
    });

    this.map.addControl(new mapboxgl.NavigationControl());
    this.draw = new MapboxDraw({
      // drawing: true,
      displayControlsDefault: false,
    });
    this.map.addControl(this.draw, 'top-left');
    this.map.on('draw.create', (e: any) => {
      this.createArea(e);
    });
    this.map.on('draw.update', (e: any) => {
      this.updateArea(e);
    });
  }

  updateArea($e: any): void {

    setTimeout(() => {
      this.geojson = this.draw.getAll();
      this.features = this.geojson.features;
    }, 1000);
  }

  createArea($e: any): void {
    this.feature = $e.features[0];
    this.name_save_button = true;

  }

  enableDraw($event: any): void {
    this.geojson_name = '';
    this.draw.changeMode('draw_polygon');
  }

  onSaveName($event: any): void {
    this.name_save_button = false;
    // @ts-ignore
    const {id} = this.feature;
    // @ts-ignore
    this.feature = {...this.feature, properties: {name: this.geojson_name}};
    this.features.push(this.feature);
    this.draw.setFeatureProperty(id, 'name', this.geojson_name);

  }

  onEdit($event: any): void {
    this.draw.changeMode('direct_select', {featureId: $event});
  }

  onDelete($event: any): void {
    this.draw.delete($event);
    setTimeout(() => {
      this.geojson = this.draw.getAll();
      this.features = this.geojson.features;
    }, 1000);

  }

}
